<?php
    class Message {
        protected $text = "A simple message";
        public static $count = 0;
        public function show(){
            echo "<p>$this->text</p>";
        }
        function __construct($text = ""){
            ++self::$count;
            if($text !=""){
                $this->text = $text;
            }
        }
    }
//הרצאה2
class redMessage extends Message {
    public function show(){
        echo "<p style = 'color:red'>$this->text</p>";

    }
}

class coloredMessage extends Message{
    protected $color='red'; // צבע דיפולטיבי
    public function __set($property,$value){  //סתר
        if($property =='color'){ //האם שם התכונה הוא color
            $colors = array('red','yellow','green');
            if(in_array($value,$colors)){
                $this->color = $value;
            }

         }
    }
    public function show(){
        echo "<p style = 'color: $this->color'>$this->text</p>";
    }

    #comment1
    #add more code
    #added code for commit 4
}
function showObject($object){
    $object->show();


    
}
?>